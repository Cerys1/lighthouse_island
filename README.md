# README #

Hello there, nice person who decided to check out this game o/

### What is this game? ###

Well it's a 2D pixelart top down game where you have to survive and escape from an island.

### What is it using? ###

The game is made in Unity 2020.3.15f2 and uses Bolt/Ludiq for the code.

### Who made it? ###

* Boyan Dobrev - code and design
* Jana Scheibner - art